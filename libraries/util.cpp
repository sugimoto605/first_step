//
//  util.cpp
//  mix4
//
//  Created by Hiroshi Sugimoto on 2015/11/09.
//  Copyright © 2015年 Hiroshi Sugimoto. All rights reserved.
//

#include "util.hpp"

double good_number(double x,bool larger){//倍精度の数xに美意識を与える
	if (x==0) return x;
	bool neg=x<0;
	if (neg) x=-x;
    double xorder=std::pow(10.0,floor(std::log10(x)));
    int ix;
    if (larger) {
        ix=ceil(x/xorder);
        switch (ix) {//      最初が1,2,4,5,8の数に比べると, それ以外は醜い数である
            case (3):       ix=4;   break;
            case (6):       ix=8;   break;
            case (7):       ix=8;   break;
            case (9):       ix=10;  break;
        }
    } else {
        ix=floor(x/xorder);
        switch (ix) {//      最初が1,2,4,5,8の数に比べると, それ以外は醜い数である
            case (3):       ix=2;   break;
            case (6):       ix=5;   break;
            case (7):       ix=5;   break;
            case (9):       ix=8;   break;
        }
    }
	if (neg) return -ix*xorder;
    return ix*xorder;
}
std::string my_ip(){//自分のIPアドレスを取得
    struct ifaddrs *ifa_list;
    char addrstr[256];
    std::string ip;
    if (getifaddrs(&ifa_list) < 0) return "";
    for (auto ifa = ifa_list; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr->sa_family == AF_INET){//     IPV4
            if (strstr(ifa->ifa_name,"lo")==NULL) {
                memset(addrstr, 0, sizeof(addrstr));
                inet_ntop(AF_INET,&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr,
                          addrstr, sizeof(addrstr));
                ip=addrstr;
            }
        }
    };
    freeifaddrs(ifa_list);
    return ip;
}
int prepare_folder(FS::path folder,bool group){
	namespace FS=boost::filesystem;
    boost::system::error_code error;
	//すでに存在するフォルダーは作成しない
    if (FS::is_directory(folder,error)) {
		//他のユーザーが作成したフォルダーは操作できにゃい
		//if (group) {
		//	FS::permissions(folder,FS::add_perms|FS::group_read|FS::group_write,error);
		//	if (error.value()) {
		//		throw(std::runtime_error("Can not set permission::" + folder.string() + ". " + error.category().name()));
		//		return EXIT_t::ERR_MKDIR;
		//	}
		//}
		return 0;
	}
	//フォルダーではないのに存在するってこた，そりゃファイルぢゃん無理〜
    if (FS::exists(folder,error)){
        throw(std::runtime_error("Can not create folder::" + folder.string() + ". File exists."));
        return 1;
    }
	//ではフォルダーを作成
    if (!FS::create_directories(folder,error)) {
		throw(std::runtime_error("Can not set permission::" + folder.string() + ". " + error.category().name()));
        return 1;
    }
	if (group) {
		FS::permissions(folder,FS::add_perms|FS::group_read|FS::group_write,error);
		if (error.value()) {
			throw(std::runtime_error("Can not set permission::" + folder.string() + ". " + error.category().name()));
			return 1;
		}
	}
    return 0;
};
std::string Quote(std::string S){
    return ("\"" + S + "\"");
}
std::string Replace( std::string String1, std::string String2, std::string String3 ) {
    std::string::size_type  Pos( String1.find( String2 ) );
    while( Pos != std::string::npos ) {
        String1.replace( Pos, String2.length(), String3 );
        Pos = String1.find( String2, Pos + String3.length() );
    }
    return String1;
};
void who_am_i(char *buf,size_t sz){
    uid_t    euid=geteuid();
    struct  passwd *pwent=getpwuid(euid);
    strncpy(buf,pwent->pw_name,sz);
};
std::string who_am_i(){
	char uname[256];
	who_am_i(uname,256);
	std::string username=uname;
	return username;
}
