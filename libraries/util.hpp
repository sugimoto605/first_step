//
//  util.hpp
//  mix3   なんだか知らんが, 古来より伝わる者たち
//
//  Created by Hiroshi Sugimoto on 2015/11/09.
//  Copyright © 2015年 Hiroshi Sugimoto. All rights reserved.
//

#ifndef util_hpp
#define util_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <sys/types.h>              //  geteuid
#include <pwd.h>                    //  getpwuid
#include <string.h>
#include <netinet/in.h>				//  networking
#include <arpa/inet.h>  				//  inet_ntoa
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <cmath>

namespace FS=boost::filesystem;
double good_number(double X,bool B=true);
std::string my_ip();
int prepare_folder(FS::path folder,bool group=false);
std::string Quote(std::string S);
std::string Replace( std::string String1, std::string String2, std::string String3 );
void who_am_i(char *buf,size_t sz);
std::string who_am_i();

#endif /* util_hpp */
