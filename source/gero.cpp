//
//  gero.cpp
//
//  Created by Hiroshi Sugimoto on 2020/09/06.
//  Copyright © 2020 Hiroshi Sugimoto. All rights reserved.
//

#include "gero.hpp"

gero::gero(unsigned int _dim):x_size(_dim){
	std::cout << "GERO Version 1.0";
	std::cout << " on " << boost::asio::ip::host_name();
	std::cout << std::endl;
	std::cout << " x_size=" << x_size;
	y_size=x_size;
	std::cout << " y_size=" << y_size << std::endl;
	FS::path pwd=FS::current_path();
	std::cout << " current path is " << pwd << std::endl;
};

void gero::test_eigen(){
	Eigen::MatrixXd A(5,3);
	A << 2.5, 3.1, 4.2,
	5.2, 3.3, 1.0,
	3.1, 5.2, 1.2,
	-1.2,4.2, std::sqrt(2.5),
	5.3, 0.2, 5.6;
	std::cout << "MATRIX A" <<std::endl;
	std::cout << A << std::endl;
	std::cout << "A.row(3)" <<std::endl;
	std::cout << A.row(3) << std::endl;
	std::cout << "A.col(1)" <<std::endl;
	std::cout << A.col(1) << std::endl;
	std::cout << "MATRIX B=A.transpose" <<std::endl;
	Eigen::MatrixXd B(3,5);
	B=A.transpose();
	std::cout << "MATRIX D=AB" <<std::endl;
	auto D=A*B;
	std::cout << "DET= " << D.determinant() << std::endl;
}
