//
//  gero.hpp
//  BGK
//
//  Created by Hiroshi Sugimoto on 2020/09/05.
//  Copyright © 2020 Hiroshi Sugimoto. All rights reserved.
//

#ifndef gero_hpp
#define gero_hpp
//基本
#include <iostream>
#include <iomanip>
#include <vector>
//Boost
#include <boost/filesystem.hpp>
#include <boost/asio.hpp>
//Eigen
#include <Eigen/Dense>

namespace FS=boost::filesystem;
class gero {//お試しクラス
private:
	unsigned int x_size;
	unsigned int y_size;
public:
	class point {
	public:
		double x[3];
	};
	std::vector<point> points;
	gero(unsigned int _dim);
	~gero() { std::cout << "GERO [" << x_size <<"] ended. Bye" << std::endl;};
	void test_eigen();//Eigenをテスト
};

#endif /* gero_hpp */
